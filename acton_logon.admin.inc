<?php
/**
 * @file
 * Administrative UI.
 */

/**
 * Basic settings form.
 */
function acton_logon_admin_settings() {
  $form['acton_logon_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Intercept Drupal login form'),
    '#default_value' => variable_get('acton_logon_form', TRUE),
    '#description' => t('Check to use Acton Logon for the regular Drupal login form.'),
  );

  $form['acton_logon_http'] = array(
    '#type' => 'checkbox',
    '#title' => t('Accept HTTP authorization'),
    '#default_value' => variable_get('acton_logon_http', TRUE),
    '#description' => t('Check to enable user authentication using HTTP Basic authorization.'),
  );

  $providers = acton_logon_get_providers();
  $form['order'] = array(
    '#type' => 'fieldset',
    '#title' => t('Re-order providers'),
    '#description' => t('Configure the order in which authentication providers will be tried. When a user successfully logs in through a provider, the provider will be mapped to the account. Thereafter, mapped providers are tried first, and then the remaining providers are tried in the following order.'),
  );
  $form['order']['acton_logon_order'] = array(
    '#theme' => 'acton_logon_admin_provider_order',
    '#id' => 'acton-logon-order-table',
    '#tree' => TRUE,
  );
  $order = variable_get('acton_logon_order', array());
  foreach ($providers as $module => $provider) {
    $form['order']['acton_logon_order'][$module]['title'] = array(
      '#markup' => check_plain($provider['label']),
    );
    $form['order']['acton_logon_order'][$module]['weight'] = array(
      '#type' => 'weight',
      '#delta' => count($providers),
      '#default_value' => isset($order[$module]) ? $order[$module] : 0,
      '#array_parents' => array('acton_logon_order', $module),
      '#attributes' => array('class' => array('acton-logon-order-module-weight')),
    );
  }
  $form['order']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset to default order'),
    '#submit' => array('acton_logon_admin_provider_order_reset'),
    '#access' => !empty($providers),
  );
  $form['acton_logon_order']['#attached']['drupal_add_tabledrag'][] = array('acton-logon-order-table', 'order', 'sibling', 'acton-logon-order-module-weight');

  $form['acton_logon_fallback'] = array(
    '#type' => 'checkbox',
    '#title' => t('Always fall back to Drupal account password'),
    '#default_value' => variable_get('acton_logon_fallback', FALSE),
    '#description' => t('Check to use the default Drupal account password regardless of whether an authentication provider has been assigned to an account. If unchecked, only the providers through which a user has logged in at least once will be used, i.e. using the Drupal account password will not work.'),
  );
  $form['acton_logon_change_pass_help'] = array(
    '#type' => 'textarea',
    '#title' => t('Override change password help text'),
    '#default_value' => variable_get('acton_logon_change_pass_help', ''),
    '#description' => t('Specify text to override the default help text for changing password on the account form. You may use token replacement patterns. Specify %none to remove the help text. Leave blank to use the Drupal system default text.', array('%none' => '<none>')),
  );
  if (module_exists('token')) {
    $form['token_help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacement patterns'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['token_help']['tree'] = array(
      '#markup' => theme('token_tree'),
    );
  }

  $form['#submit'][] = 'acton_logon_admin_settings_submit';
  $form = system_settings_form($form);
  $form['#submit'][] = 'acton_logon_admin_rebuild';

  return $form;
}

/**
 * Themes a table for ordering providers.
 */
function theme_acton_logon_admin_provider_order($variables) {
  $element = $variables['element'];
  $element += array('#attributes' => array());
  $element['#attributes']['id'] = $element['#id'];
  $attributes = $element['#attributes'];

  // Build table.
  $table = array(
    'header' => array(),
    'rows' => array(),
    'empty' => t('There are no authentication providers in enabled modules.'),
    'attributes' => $attributes,
  );
  foreach (element_children($element) as $module) {
    $element[$module] += array('#attributes' => array());
    $element[$module]['#attributes']['id'] = $element[$module]['#id'];
    $row = array();
    foreach (element_children($element[$module]) as $column) {
      $row[] = drupal_render($element[$module][$column]);
    }
    $row = array(
      'data' => $row,
    );
    $row += $element[$module]['#attributes'];
    $row['class'][] = 'draggable';
    $table['rows'][] = $row;
  }

  return theme('table', $table);
}

/**
 * Reset ordering.
 */
function acton_logon_admin_provider_order_reset() {
  variable_del('acton_logon_order');
  drupal_set_message(t('Ordering for authentication providers has been reset.'));
}

/**
 * Handles basic settings form submission.
 */
function acton_logon_admin_settings_submit($form, &$form_state) {
  $form_state['values'] += array('acton_logon_order' => array());
  foreach ($form_state['values']['acton_logon_order'] as $module => &$value) {
    $value = $value['weight'];
  }
}

/**
 * Form submit handler to rebuild provider info.
 */
function acton_logon_admin_rebuild() {
  acton_logon_get_providers(TRUE);
}

/**
 * Per-provider settings form.
 */
function acton_logon_admin_provider_settings() {
  $providers = acton_logon_get_providers();

  $settings = variable_get('acton_logon_provider_settings', array());
  $form['acton_logon_provider_settings'] = array(
    '#type' => 'vertical_tabs',
    '#tree' => TRUE,
  );
  foreach ($providers as $module => $provider) {
    $settings += array($module => array());
    $settings[$module] += array('match' => '', 'exclude' => '');
    $form['acton_logon_provider_settings'][$module] = array(
      '#type' => 'fieldset',
      '#title' => $provider['label'],
    );
    $form['acton_logon_provider_settings'][$module]['match'] = array(
      '#type' => 'textarea',
      '#title' => t('Username pattern'),
      '#default_value' => $settings[$module]['match'],
      '#element_validate' => array('acton_logon_admin_sanitize_pattern'),
      '#description' => t('Enter case-sensitive !regex, one per line, to match usernames to authenticate with %provider. If no pattern is given, all usernames are matched. Tip: use caret (^) and dollar sign ($) to mark string boundaries to avoid spaces from being automatically trimmed on saving.', array('!regex' => l(t('regular expression'), 'http://www.regular-expressions.info/', array('attributes' => array('target' => '_blank'))), '%provider' => $provider['label'])),
    );
    $form['acton_logon_provider_settings'][$module]['exclude'] = array(
      '#type' => 'textarea',
      '#title' => t('Exclude pattern'),
      '#default_value' => $settings[$module]['exclude'],
      '#element_validate' => array('acton_logon_admin_sanitize_pattern'),
      '#description' => t('Enter case-sensitive !regex, one per line, to match usernames to exclude from %provider. If no pattern is given, no username is excluded. Tip: use caret (^) and dollar sign ($) to mark string boundaries to avoid spaces from being automatically trimmed on saving.', array('!regex' => l(t('regular expression'), 'http://www.regular-expressions.info/', array('attributes' => array('target' => '_blank'))), '%provider' => $provider['label'])),
    );
  }

  return system_settings_form($form);
}

/**
 * Sanitizes the regular expression pattern input.
 */
function acton_logon_admin_sanitize_pattern($element, &$form_state) {
  $value = $element['#value'];
  $patterns = array();
  $raw_patterns = preg_split('/[\r\n]+/', trim($value));
  foreach ($raw_patterns as $raw_pattern) {
    if (strlen($pattern = trim($raw_pattern)) > 0) {
      $patterns[] = $pattern;
    }
  }
  $value = implode("\n", $patterns);
  drupal_array_set_nested_value($form_state['values'], $element['#parents'], $value);
}

/**
 * Login form settings form.
 */
function acton_logon_admin_form_settings() {
  $form['acton_logon_form_user_help'] = array(
    '#type' => 'textarea',
    '#title' => t('Override username help text'),
    '#default_value' => variable_get('acton_logon_form_user_help', ''),
    '#description' => t('Specify text to override the default username help text. You may use token replacement patterns. Specify %none to remove the help text. Leave blank to use the Drupal system default text.', array('%none' => '<none>')),
  );
  $form['acton_logon_form_pass_help'] = array(
    '#type' => 'textarea',
    '#title' => t('Override password help text'),
    '#default_value' => variable_get('acton_logon_form_pass_help', ''),
    '#description' => t('Specify text to override the default password help text. You may use token replacement patterns. Specify %none to remove the help text. Leave blank to use the Drupal system default text.', array('%none' => '<none>')),
  );
  $form['acton_logon_form_user_error'] = array(
    '#type' => 'textarea',
    '#title' => t('Override username error message'),
    '#default_value' => variable_get('acton_logon_form_user_error', ''),
    '#description' => t('Specify message to override the default username error message. You may use token replacement patterns. Specify %none to remove the help text. Leave blank to use the Drupal system default text.', array('%none' => '<none>')),
  );  
  if (module_exists('token')) {
    $form['token_help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacement patterns'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['token_help']['tree'] = array(
      '#markup' => theme('token_tree'),
    );
  }

  return system_settings_form($form);
}

/**
 * Basic HTTP login settings form.
 */
function acton_logon_admin_http_settings() {
  $form['callback_help'] = array(
    '#prefix' => '<p>',
    '#markup' => t('You can use the path "%callback" (without quotes) to prompt for HTTP authentication. This can be done without checking the box below. This is useful for setting the option %site_403.', array('%callback' => 'acton_logon_http_login', '%site_403' => t('Default 403 (access denied) page'))),
    '#suffix' => '</p>',
  );

  $form['acton_logon_force'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require login'),
    '#default_value' => variable_get('acton_logon_force', FALSE),
    '#description' => t('Check to force the user to authenticate.'),
  );
  $form['basic_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authorization prompt'),
    '#states' => array(
      'visible' => array(
        ':checkbox[name=acton_logon_force]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['basic_config']['acton_logon_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Prompt message'),
    '#default_value' => variable_get('acton_logon_message', 'Restricted access'),
    '#description' => t('Enter a short message accompanying the prompt requesting user credentials.'),
  );
  $form['basic_config']['acton_logon_unauthorized_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Unauthorized page title'),
    '#default_value' => variable_get('acton_logon_unauthorized_title', t('Unauthorized')),
    '#description' => t('Enter the title of the page displayed when a user is not authorized.'),
  );
  $form['basic_config']['acton_logon_unauthorized_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Unauthorized page body'),
    '#default_value' => variable_get('acton_logon_unauthorized_body', t('You are not authorized to access this page.')),
    '#description' => t('Enter the body of the page displayed when a user is not authorized. You may enter HTML markup.'),
  );

  return system_settings_form($form);
}

/**
 * HTTP guest login settings form.
 */
function acton_logon_admin_http_guest_settings() {
  $acton_logon_force = variable_get('acton_logon_force');
  $form['acton_logon_guest'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use guest login'),
    '#default_value' => $acton_logon_force && variable_get('acton_logon_guest', FALSE),
    '#description' => t('Check to enable HTTP guest login as anonymous users. The "Require login" option in HTTP must be selected.'),
    '#states' => array(
      'visible' => array(
        ':checkbox[name=acton_logon_force]' => array('checked' => TRUE),
      ),
    ),
    '#disabled' => empty($acton_logon_force),
  );
  $form['_pseudo_acton_logon_force'] = array(
    '#markup' => sprintf('<input type="hidden" name="acton_logon_force" value="%s" />', variable_get('acton_logon_force')),
  );
  $form['guest'] = array(
    '#type' => 'fieldset',
    '#title' => t('Guest login'),
    '#states' => array(
      'visible' => array(
        'input[name=acton_logon_force]' => array('value' => 1),
        ':checkbox[name=acton_logon_guest]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['guest']['acton_logon_guest_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Guest username'),
    '#default_value' => variable_get('acton_logon_guest_username'),
  );
  $form['guest']['acton_logon_guest_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Guest password'),
    '#default_value' => variable_get('acton_logon_guest_password'),
  );

  $form = system_settings_form($form);
  $form['actions']['#access'] = !empty($acton_logon_force);

  return $form;
}