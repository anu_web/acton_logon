<?php
/**
 * @file
 * Administrative UI.
 */

/**
 * Settings form.
 */
function acton_logon_ldap_admin_settings() {
  $form['acton_logon_ldap_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory URI'),
    '#default_value' => variable_get('acton_logon_ldap_uri'),
    '#description' => t('Specify the URI of the LDAP server, formatted with the prefix "ldap://" or "ldaps://" (without quotes).'),
  );
  $form['acton_logon_ldap_binddn'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory bind DN'),
    '#default_value' => variable_get('acton_logon_ldap_binddn'),
    '#description' => t('Specify the pattern for the Distinguished Name (DN) to use for binding to the LDAP directory. Use the placeholder %username to place the username used to authenticate.', array('%username' => '%{username}')),
  );

  return system_settings_form($form);
}

/**
 * Extracts user names from a multi-line textarea.
 */
function acton_logon_ldap_admin_extract_user_names($element, &$form_state) {
  $raw_value = $element['#value'];
  $value = array();
  foreach (preg_split('/[\r\n]+/', $raw_value) as $name) {
    if (strlen($name = trim($name)) > 0) {
      $value[] = $name;
    }
  }
  form_set_value($element, $value, $form_state);
}
